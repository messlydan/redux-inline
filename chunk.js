const cfg    = require( "./cfg" );
const action = require( "./action" );

const lens = ( key ) => ( state ) => {

    return state[ cfg.STATE_ROOT ][ key ];

};

const get = ( key ) => ( dispatch, getState ) => {

    return lens( key )( getState() );

};

const set = ( key ) => ( partialChunk, type ) => ( dispatch, getState ) => {
    var oldChunk = lens( key )( getState() );
    var newChunk = { ... oldChunk, ... partialChunk };

    dispatch( action.update( {
        type,
        key,
        data  : newChunk
    } ) );

};

const reset = ( key ) => ( resetChunk = {}, type ) => ( dispatch ) => {

    dispatch( action.update( {
        type,
        key,
        data  : resetChunk
    } ) );

};

module.exports = { get, lens, set, reset };
