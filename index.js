const chunk  = require( "./chunk" );
const cfg    = require( "./cfg" );

const build = ( key ) => {

    return {
        lens  : chunk.lens( key ),
        get   : chunk.get( key ),
        reset : chunk.reset( key ),
        set   : chunk.set( key )
    };

};

const reducer = ( state = {}, action ) => {

    // if( action.type === cfg.ACTION_TYPE )
        return {
            ... state,
            [ action.key ] : action.data
        };

    // return state;

};

const middleware = ( { dispatch, getState } ) => next => action => {

    if( typeof action === "function" )
        return action( dispatch, getState );

    return next( action );

};

module.exports = { build, reducer, cfg, middleware };
