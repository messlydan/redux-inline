const cfg = require( "./cfg" );

const update = ( { key, data, type } ) => ({
    type : type || cfg.ACTION_TYPE,
    key  : key,
    data : data
});

module.exports = { update };
