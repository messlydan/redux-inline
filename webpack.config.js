const path = require( "path" );
module.exports = {

    entry : "./index.js",

    output : {
        path          : path.resolve( __dirname, "dist/" ),
        filename      : "redux-inline.js",
        library       : "ReduxInline",
        libraryTarget : "umd",
        globalObject  : "this"
    },

    module :  { 

        rules : [{
            test : /\.js$/,
            use  : [ "babel-loader" ],
        }]

    }
};
