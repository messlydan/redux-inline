Redux Inline
============

A front-end state management library that is powered by Redux.

## Purpose of this library

When writing front-ends in Redux, I've found my code is split across the following domains:

  * Action creators.
  * Reducers.
  * Thunks.

My (subjective) opinion is this that leads to code that is harder to grok. Developers must jump between multiple functions and/or files to understand how the state is being transformed. I believe this negative outweights any potential benefit of rigidly organizing the code into the aformentioned 3 domains.

The purpose of this library is to allow inline state transformations within thunks (effectively eliminating the need for explicitly defined actions and reducers), whilst sitting atop Redux and thus benefiting from the large ecosystem of tools and libraries that exist around it.

## How does it work

Redux Inline assumes front-end state is structured into a flat object of uniquely keyed *chunks*. Each chunk can be transformed and inspected within a thunk by the following atomic methods:

  * `get       :: chunkKey => (dispatch, getState) => state` : Grabs a current snapshot of the state of the chunk.
  * `set       :: chunkKey => partialState => (dispatch, getState) => ()` : Merges the current state of the chunk with the `partialState` provided.
  * `reset     :: chunkKey => newState => (dispatch, getState) => ()` : Replaces the current state of the chunk with the `newState` provided.

A special `build` method exists which returns the aforementioned functions, but with the first `chunkKey` argument applied - essentially binding the functions to a specific chunk. 

### Lensing

A small convenience method called `lens` is also returned by `build`. It is useful within the `mapStateToProps` function when using `react-redux`. It takes the Redux root state object and returns the state of the chunk.

## A quick example

```javascript
const { createStore, applyMiddleware, combineReducers } = require( "redux" );
const { cfg, reducer, middleware, build } = require( "redux-inline" );

const CHUNK_KEY = "chunk";
const { get, set, reset, lens } = build( CHUNK_KEY );

const incrementCounter = ( ... ctx ) => {
    var count = get( ... ctx ).count;
    set( { count : count + 1 } )( ... ctx );
};

const initCounter = ( ... ctx ) => {
    reset( { count : 0, otherKey : "some random text" } )( ... ctx );
};

const initThenIncrementThenHalve = ( n ) => ( ... ctx ) => {
    initCounter( ... ctx );
    for( var i = 0; i < n; i +=1 )
        incrementCounter( ... ctx );
    var { count } = get( ... ctx );
    set( { count : count / 2 } )( ... ctx );
};

const store = createStore( 
    combineReducers( { [ cfg.STATE_ROOT ] : reducer } ),
    applyMiddleware( middleware ) // Can be omitted if already using "redux-thunk" middleware.
);

// Print the state of the chunk after each change.
store.subscribe( () => console.log( lens( store.getState() ) ) );

store.dispatch( initThenIncrementThenHalve( 50 ) );
```
